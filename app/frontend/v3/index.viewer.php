<?php $v->import('header') ?>
      <nav class="links">
        <?php foreach($v->links as $k => $elm):?>
          <?php if(count($elm->dropdown) > 0): ?>
          <div class="dropdown">
          <?php endif; ?>
          <a class="button" href="<?php echo $elm->url ?>" title="<?php echo $elm->title?>"<?php if(!empty($elm->attrs)):
              foreach ($elm->attrs as $attr_name => $attr_value):
                echo $attr_name.'="'.$attr_value.'"';
              endforeach;
            endif;
        ?> <?php if(count($elm->dropdown) > 0):?>
            data-target="<?php echo $elm->title?>-dropdown"
       <?php endif;?>
        >
            <?php echo $elm->title ?>&nbsp;
            <?php if(count($elm->dropdown) > 0):?>
             <i class="fa fa-caret-down "></i>
          <?php endif;?>
      </a>

            <?php if(count($elm->dropdown) > 0):?>
                  <div class="dropdown-menu" id="<?php echo $elm->title?>-dropdown">
                  <?php for($i = 0;$i < count($elm->dropdown);$i++):?>

                       <a href="<?php echo $elm->dropdown[$i]->url ?>" target="_blank">
                             <i class="fa fa-<?php echo $elm->dropdown[$i]->icon?>"></i>&nbsp;
                             <?php echo $elm->dropdown[$i]->title ?>
                       </a>
                 <?php endfor;?>
                 </div>
           </div>
            <?php endif; ?>
        <?php endforeach; ?>
      </nav>
      <nav class="social">
          <?php foreach($v->social_media as $elm): ?>
                <a target="_blank" href="<?php echo $elm->url?>" class="fa fa-<?php echo $elm->icon ?> fa-2x"></a>
          <?php endforeach ?>
      </nav>
<?php $v->import('footer') ?>
