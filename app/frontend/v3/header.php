<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php echo $v->personal->name ?></title>
    <meta name="description" content="<?php echo $v->personal->des ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="google-site-verification" content="VlX6BEA4EeGqQwjhu6kuDPROpzbr5Z7a_T2C1z_8JAg" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@reThinker001" />
    <meta name="twitter:creator" content="@reThinker001" />
    <meta name="twitter:title" content="<?php echo $v->personal->name ?>" />
    <meta name="twitter:description" content="<?php echo $v->personal->des ?>" />
    <meta name="twitter:image" content="https://secure.gravatar.com/avatar/f034620aa53dc048163eb8b1bd7a48a7?s=150" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <?php $v->asset('main','css'); ?>

  </head>
  <body>
    <div class="container">
      <div class="personal">
        <h1 style="margin-bottom:5px;"><?php echo $v->personal->name ?></h1>
        <quote>- "<?php echo $v->personal->des ?>"</quote>
      </div>
