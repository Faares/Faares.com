<?php

return [

	/* Site Url */
	'SITE_URL'=>'http://Faares.com',

	'ASSETS_URL'=>'/assets',

	/* Theme name */
	'THEMENAME'=>'v3',

	/* Base Dir */
	'DIR_BASE'=>realpath('./'),

	/* App Dir */
	'DIR_APP'=>realpath('./app/'),

	/* Tools Dir */
	'DIR_TOOLS'=>realpath('./app/tools'),

	/* Processors Dir */
	'DIR_PROCESSORS'=>realpath('./app/processors'),

	/* Frontend dir (Viewers) */
	'DIR_FRONTEND'=>realpath('./app/frontend'),

	/* Uploads Dir */
	'DIR_UPLOADS'=>realpath('./app/Uploads'),

	'DIR_ROUTERS'=>realpath('./app/routers'),

	'DIR_DATABASE'=>realpath('./app/database'),

	/* Database Data */
	'DB_HOST'=>'localhost',

	'DB_USER'=>'root',

	'DB_PASS'=>'',

	'DB_NAME'=>'',

	'DEBUG_MODE'=>false

];
